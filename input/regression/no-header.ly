\version "2.12.0"

\markuplines \wordwrap-lines {
  This regtest does not contain any header and paper blocks. Its purpose is to
  test whether anything breaks if these blocks are absent.
}
