@c -*- coding: utf-8; mode: texinfo; documentlanguage: fr -*-

@ignore
    Translation of GIT committish: 057106293b07b74b00553fe4dc3dfac5c1f3b682

    When revising a translation, copy the HEAD committish of the
    version that you are working on.  For details, see the Contributors'
    Guide, node Updating translation committishes..
@end ignore

@c \version "2.13.36"

@c Translators: Jean-Charles Malahieude

@node Modèles
@appendix Modèles
@translationof Templates

Cette annexe du manuel d'initiation propose des patrons de partition
Lilypond, prets à l'emploi.  Il vous suffira d'y ajouter quelques notes,
de lancer LilyPond, et d'aprécier le résultat.


@menu
* Portée unique::
* Modèles pour piano::
* Quatuor à cordes::
* Ensemble vocal::
* Orchestre::
* Exemples de notation ancienne::
* Autres modèles::
@end menu

@node Portée unique
@appendixsec Portée unique
@translationof Single staff

@appendixsubsec Notes seules

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc,addversion]
{single-staff-template-with-only-notes.ly}

@appendixsubsec Notes et paroles

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc,addversion]
{single-staff-template-with-notes-and-lyrics.ly}

@appendixsubsec Notes et accords

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{single-staff-template-with-notes-and-chords.ly}

@appendixsubsec Notes, paroles et accords

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{single-staff-template-with-notes,-lyrics,-and-chords.ly}


@node Modèles pour piano
@appendixsec Modèles pour piano
@translationof Piano templates

@appendixsubsec Piano seul

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{piano-template-simple.ly}

@appendixsubsec Chant et accompagnement

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{piano-template-with-melody-and-lyrics.ly}

@appendixsubsec Piano et paroles entre les portées

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{piano-template-with-centered-lyrics.ly}

@appendixsubsec Piano et nuances entre les portées

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{piano-template-with-centered-dynamics.ly}


@node Quatuor à cordes
@appendixsec Quatuor à cordes
@translationof String quartet

@appendixsubsec Quatuor à cordes

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{string-quartet-template-simple.ly}

@appendixsubsec Parties pour quatuor à cordes

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{string-quartet-template-with-separate-parts.ly}


@node Ensemble vocal
@appendixsec Ensemble vocal
@translationof Vocal ensembles

@appendixsubsec Partition pour chœur à quatre voix mixtes

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{vocal-ensemble-template.ly}

@appendixsubsec Partition pour chœur SATB avec réduction pour piano

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{vocal-ensemble-template-with-automatic-piano-reduction.ly}

@appendixsubsec Partition pour chœur SATB avec alignement des contextes

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{vocal-ensemble-template-with-lyrics-aligned-below-and-above-the-staves.ly}

@appendixsubsec Partition pour chœur SATB, sur quatre portées

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{satb-choir-template---four-staves.ly}

@appendixsubsec Couplet pour solo et refrain à deux voix

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{vocal-ensemble-template-with-verse-and-refrain.ly}

@appendixsubsec Hymnes et cantiques

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{hymn-template.ly}

@appendixsubsec Psalmodie

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{anglican-psalm-template.ly}


@node Orchestre
@appendixsec Orchestre
@translationof Orchestral templates

@appendixsubsec Orchestre, chœur et piano

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{orchestra,-choir-and-piano-template.ly}


@c bad node name to avoid node name conflict
@node Exemples de notation ancienne
@appendixsec Exemples de notation ancienne
@translationof Ancient notation templates

@appendixsubsec Transcription de musique mensurale

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{ancient-notation-template----modern-transcription-of-mensural-music.ly}

@appendixsubsec Transcription du grégorien

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{ancient-notation-template----modern-transcription-of-gregorian-music.ly}


@node Autres modèles
@appendixsec Autres modèles
@translationof Other templates

@appendixsubsec Symboles de jazz
@translationof Jazz combo

@lilypondfile[verbatim,lilyquote,ragged-right,texidoc]
{jazz-combo-template.ly}

